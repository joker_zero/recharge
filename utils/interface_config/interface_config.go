/***************************************************
 ** @Desc : This file for 对接上游固定参数
 ** @Time : 2019.04.08 10:03
 ** @Author : Joker
 ** @File : address
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.08 10:03
 ** @Software: GoLand
****************************************************/
package interface_config

//先锋
const (
	HOST                   = ""
	SECID                  = "RSA"
	VERSION                = "4.0.0"
	XF_QUERY_URL           = "https://mapi.ucom/gateway.do"
	XF_RECHANGE_URL        = "https://mapi.uccom/gateway.do"
	XF_RECHANGE_NOTICE_URL = HOST + "notice/xf_recharge_notice/"
	XF_PAY_NOTICE_URL      = HOST + "notice/xf_pay_notice/"
	XF_TRANSFER_NOTICE_URL = HOST + "notice/b2c_transfer_notice/"

	VERSION_V2                = "5.0.0"
	XF_RECHANGE_NOTICE_URL_V2 = HOST + "notice_v2/xf_recharge_notice/"
	XF_PAY_NOTICE_URL_V2      = HOST + "notice_v2/xf_pay_notice/"
)

// 密钥对
const (
	PUBLIC_KEY  = ``
	PRIVATE_KEY = `
-----BEGIN RSA PRIVATE KEY-----

-----END RSA PRIVATE KEY-----
`
)
