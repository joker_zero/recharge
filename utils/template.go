/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019.04.03 18:01 
 ** @Author : Joker
 ** @File : template
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.03 18:01
 ** @Software: GoLand
****************************************************/
package utils

import "fmt"

type Template struct{}

//if多条件判断
//管理员权限控制
func (*Template) IfOr(r int) bool {
	if r == -1 || r == 0 {
		return true
	} else {
		return false
	}
}

// VIP用户权限控制
func (*Template) IfOrB2C(r int) bool {
	if r == -1 || r == 0 || r == 1 {
		return true
	} else {
		return false
	}
}

func (*Template) Yuan2(yuan float64) string {
	return fmt.Sprintf("%.2f", yuan)
}