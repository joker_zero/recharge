/***************************************************
 ** @Desc : This file for 对接异步通知
 ** @Time : 2019.04.22 16:18 
 ** @Author : Joker
 ** @File : api_asy_notice
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.22 16:18
 ** @Software: GoLand
****************************************************/
package implement

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"recharge/models"
	"recharge/sys"
	"recharge/utils"
	"strings"
	"time"
)

type ApiAsyNotice struct{}

var apiRPImpl = ApiRechargePayImpl{}

// 对接充值异步通知
func (*ApiAsyNotice) ApiRechargeAsyNotice(record models.RechargeRecord) {
	info, _ := userMdl.SelectOneUserById(record.UserId)

	params := models.ApiResponseBody{}
	params.MerchantNo = fmt.Sprintf("%d", record.UserId)
	params.OutTradeNo = record.ReOrderId
	params.TradeStatus = utils.GetApiOrderStatus()[record.Status]
	params.TradeNo = record.SerialNumber
	params.TradeTime = record.EditTime
	params.Item = record.Item

	fee := info.RechargeFee + record.ReAmount*info.RechargeRate*0.001
	params.Amount = globalMethod.MoneyYuanToString(record.ReAmount)
	params.AmountNotFee = globalMethod.MoneyYuanToString(record.ReAmount - fee)
	params.AmountFee = globalMethod.MoneyYuanToString(fee)

	params.ResultCode = "0000"
	params.Msg = record.Remark

	sign := apiRPImpl.GenerateSignV1(params, info.ApiKey)
	params.Sign = sign

	//生成请求参数
	toMap := globalMethod.StructToMap(params)
	byMap := globalMethod.UrlEncode(toMap)

	//发送请求
	urls := record.ApiNoticeUrl + "?" + byMap
	resp, err := http.Get(urls)
	if err != nil {
		sys.LogError("充值异步通知没有响应：", err, record.ApiNoticeUrl)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		sys.LogError("充值异步通知响应错误：", err)
	}
	_ = resp.Body.Close()

	// 重新通知四次
	if strings.Compare(string(body), "SUCCESS") != 0 {
		now := time.Now()
		add := now.Add(time.Minute * 5)
		i := 0
		for {
			tm := time.NewTicker(add.Sub(now))
			if i == 4 {
				tm.Stop()
				break
			}

			//发送请求
			urls := record.ApiNoticeUrl + "?" + byMap
			resp2, err2 := http.Get(urls)
			if err2 != nil {
				sys.LogError("充值异步通知没有响应：", err2)
			}
			body2, err2 := ioutil.ReadAll(resp.Body)
			if err2 != nil {
				sys.LogError("充值异步通知响应错误：", err2)
			}
			if strings.Compare(string(body2), "SUCCESS") == 0 {
				tm.Stop()
				break
			}
			_ = resp2.Body.Close()

			// 定时
			add = add.Add(time.Minute * 4)
			// 23 29 39 53
			<-tm.C
			i++
		}
	}
}

/// 对接代付异步通知
func (*ApiAsyNotice) ApiPayAsyNotice(record models.WithdrawRecord) {
	info, _ := userMdl.SelectOneUserById(record.UserId)

	params := models.ApiResponseBody{}
	params.MerchantNo = fmt.Sprintf("%d", record.UserId)
	params.OutTradeNo = record.WhOrderId
	params.TradeStatus = utils.GetApiOrderStatus()[record.Status]
	params.TradeNo = record.SerialNumber
	params.TradeTime = record.EditTime
	params.Item = record.Item

	params.Amount = globalMethod.MoneyYuanToString(record.WhAmount + info.PayFee)
	params.AmountNotFee = globalMethod.MoneyYuanToString(record.WhAmount)
	params.AmountFee = globalMethod.MoneyYuanToString(info.PayFee)

	params.ResultCode = "0000"
	params.Msg = record.Remark

	sign := apiRPImpl.GenerateSignV1(params, info.ApiKey)
	params.Sign = sign

	//生成请求参数
	toMap := globalMethod.StructToMap(params)
	byMap := globalMethod.UrlEncode(toMap)

	//发送请求
	urls := record.ApiNoticeUrl + "?" + byMap
	sys.LogInfo("代付异步通知url=", urls)
	resp, err := http.Get(urls)
	if err != nil {
		sys.LogError("代付异步通知没有响应：", err, record.ApiNoticeUrl)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		sys.LogError("代付异步通知响应错误：", err)
	}
	_ = resp.Body.Close()

	// 重新通知四次
	if strings.Compare(string(body), "SUCCESS") != 0 {
		now := time.Now()
		add := now.Add(time.Minute * 5)
		i := 0
		for {
			// 定时
			tm := time.NewTicker(add.Sub(now))
			if i == 4 {
				tm.Stop()
				break
			}

			//发送请求
			urls := record.ApiNoticeUrl + "?" + byMap
			resp2, err2 := http.Get(urls)
			if err2 != nil {
				sys.LogError("代付异步通知没有响应：", err2)
			}
			body2, err2 := ioutil.ReadAll(resp.Body)
			if err2 != nil {
				sys.LogError("代付异步通知响应错误：", err2)
			}
			if strings.Compare(string(body2), "SUCCESS") == 0 {
				tm.Stop()
				break
			}
			_ = resp2.Body.Close()

			// 定时间隔
			add = add.Add(time.Minute * 4)
			// 23 29 39 53
			<-tm.C
			i++
		}
	}
}
