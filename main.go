package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "recharge/models"
	_ "recharge/routers"
	"recharge/sys"
	"recharge/utils"
)

func init() {
	//初始化日志和数据库
	sys.InitSystem()

	//如果是开发模式，则显示命令信息
	isDev := !(beego.AppConfig.String("runmode") != "dev")
	if isDev {
		orm.Debug = isDev
	}
	//自动建表
	_ = orm.RunSyncdb("default", false, isDev)
}

func main() {
	//自定义模板函数
	temp := utils.Template{}
	_ = beego.AddFuncMap("IfOr", temp.IfOr)
	_ = beego.AddFuncMap("IfOrB2C", temp.IfOrB2C)
	_ = beego.AddFuncMap("Yuan2", temp.Yuan2)
	beego.Run()
}
